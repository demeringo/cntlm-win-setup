set cntlmexe=.\cntlm.exe
set cntlmconf=.\cntlm.ini

REM ------------- start proxy on local configuration file (verbose)
%cntlmexe% -v -c %cntlmconf%

REM ------------- start proxy on local configuration file (verbose) and test URL
REM %cntlmexe% -v -I -c %cntlmconf% -M http://www.google.com

REM pause to read logs if something crashes
pause



