# Quickly setup a CNTML proxy on Windows. 

Basic MS .bat wrappers and configuration instructions for the excellent cntml proxy .

_Binaries v0.92.3 are provided here as a convenience only. Please refer to the original project (http://cntlm.sourceforge.net/) for the latest version of CNTML source and binaries._

## Credits

All credits belong to David Kubicek, the he author of the original GNU-licensed project (CNTLM - Authenticating HTTP Proxy Copyright (C) 2007-2010 David Kubicek).

http://cntlm.sourceforge.net/



## Purpose

```mermaid
graph LR
  subgraph Local machine
  A1[App 1] -->|No authentication| CNTLM(CNTLM proxy + your credentials)
  A2[App 2] -->|No authentication| CNTLM
  end
  CNTLM -->|Authentication| CORP(Corporate proxy)
  CORP --> www((Internet))
  style www fill:#8FB7E5
```

Cntlm proxy runs on your Windows machine. It helps local applications (or CLI tools) access Internet via a corporate proxy that requires NTML authentication.
You still have to provide credentials to your coporate proxy (parent proxy) but expose a non NTLM proxy to your applications.

Benefits:
- you maintain your credentials in a single place (in cntlm proxy config)
- any application can acess internet, even if not able to directly authenticate using NTLM.


## Usage

- Clone/download this repository
- Configure cntml for your specific environment (steps below). You need:
  - url of your coporate proxy
  - your personal NT credentials

### Configure your proxy 

Edit `cntlm.ini`


Set your NTLM username and domain

```ini
# Example
Username my_nt_login
Domain eu
```

Replace `Proxy <YOUR-COPRORATE-PROXY>:<Your-corp-proxy-port>` by the details of your corporate proxy (parent proxy).

```ini
# Example
Proxy mycoporateproxy.company.corp.:3128
```

Precise the host (windows host) and port your proxy :

```ini
# Specify the port cntlm will listen on
# You can bind cntlm to specific interface by specifying
# the appropriate IP address also in format <local_ip>:<local_port>
# Cntlm listens on 127.0.0.1:3128 by default
#
# In this example we listen on port 9128
Listen 192.168.10.01:9128
```

TODO: does it work with hostname (for dynamic IP address) or only writting listen port ?


### Generate a password Hash and copy it to config file

To avoid storing your NT password directly in `cntml.ini`, first retrieve a hash of it.  
_You will have to redo it each time your NT password changes._

- Double click on `test_settings_and_hash_password.bat`
- Give your password on the command prompt
- Check console output, look for OK response and Copy the 2 lines containin Auth method and generated hash :
    ```cmd
    OK (HTTP code: 200)
    ----------------------------[ Profile  0 ]------
    Auth            NTLMv2
    PassNTLMv2      16AE63BExxxxxxxxxxxxxxxxxxxxxxxx
    ------------------------------------------------
    ```
- Paste the two lines to `cntlm.ini` and save the file.
    ```ini
    # NOTE: Use plaintext password only at your own risk
    # Use hashes instead. You can use a "cntlm -M" and "cntlm -H"
    # command sequence to get the right config for your environment.
    # See cntlm man page
    # Example secure config shown below.
    # PassLM          1AD35398BE6565DDB5C4EF70C0593492
    # PassNT          77B9081511704EE852F94227CF48A793
    Auth            NTLMv2
    PassNTLMv2      16AE63BExxxxxxxxxxxxxxxxxxxxxxxx
    ```


### Start or stop the proxy

- Start: double-click on `start_proxy.bat` and leave the command windows open.
- Stop: close the command window.


### Configure client applications

#### General case

Configure your applications through GUI or envt variables to point to your proxy (In this example your proxy is configured to listen to  `http://192.168.10.01:9128`)

```sh
export ftp_proxy=http://192.168.10.01:9128
export http_proxy=$ftp_proxy
export https_proxy=$ftp_proxy
```

#### Optional configuration for yum:

You may add settings permanently to yum.conf, edit `/etc/yum.conf`, then edit or add the main section.

```sh
[main]
………………
proxy=http://192.168.10.01:9128
# proxy_username=<Proxy-User-Name>
# proxy_password=<Proxy-Password> 
………………

```

### Optional cntml proxy configuration

You can set other options in your `cntml.ini` file:

- You can restrict acces to your proxy to specific IP's (See `Gateway` and `allow` `deny` parameters).
- You can configure `NoProxy` rules to avoid specif URLs to be proxied.
